<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes();

Route::get('/admin', 'AdminController@admin')
    ->middleware('is_admin')
    ->name('admin');
Route::get('/admin/loan', 'AdminController@loan')
    ->middleware('is_admin')
    ->name('admin-loan');

Route::post('/admin/loan/approve', 'AdminController@approve')
    ->middleware('is_admin')
    ->name('admin-approve');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/loan', 'LoanController@index')->name('loan');

Route::post('/loan/create', 'LoanController@create');

Route::get('/loan/list', 'LoanController@loan')->name('loan-list');
Route::post('/loan/repay', 'LoanController@repay');
