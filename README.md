# LoanBank

+ Laravel 5.7
+ run php artisan migrate
+ user admin default:
             admin@bank.com/123456
+ can register normal user to test
+ only admin can list all loans to approve
+ if normal user try to access /admin redirect to home
url admin: http://127.0.0.1:8000/admin
+ after login success normal user can go to create new loan, list his loan and repay loan
+ Some simple validation form request.
+ Ajax request for approve and repay action

Sorry if this source code is very simple, for some personal reason Im just spend few hours to build this.

Thank You and Best Regards,
Nguyen Bui


