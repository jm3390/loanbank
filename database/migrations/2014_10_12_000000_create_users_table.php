<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('type')->default('default');
            $table->rememberToken();
            $table->timestamps();
        });
        \Illuminate\Support\Facades\DB::table('users')->insert([
           'email' => 'admin@bank.com',
            'name' => 'admin',
            'password' => '$2y$10$eQD5FFJnZhr9ieso9TT/AOEOiXQMPwRGflUiv4.UrpIj1CcwnGsT2',
            'type' => 'admin'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
