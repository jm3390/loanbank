@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Loan Application</div>

                <div class="card-body">
                    <form name="loan-request" action="{{url('/loan/create')}}" method="POST">
                        @csrf
                        @if($errors->any())
                            <div class="notification is-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="row col-md-12">
                            <label class="col-md-6">Email: </label>
                            <input name="email" class="col-md-6" value="{{old('email')}}"/>
                        </div>
                        <div class="row col-md-12">
                            <label class="col-md-6">Amount require: </label>
                            <input name="amount_require" type="number" class="col-md-6" value="{{old('amount_require')}}"/>
                        </div>
                        <div class="row col-md-12 form-check-inline">
                            <input name="term" type="checkbox" />
                            <label class="">Loan Term </label>
                        </div>
                        <div class="row col-md-12">
                            <button name="submit" type="submit">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
