@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    <div class="row col-md-12">
                        <div class="col-md-3">Email</div>
                        <div class="col-md-2">Amount</div>
                        <div class="col-md-2">Approved</div>
                        <div class="col-md-2"></div>
                        <div class="col-md-1"></div>
                    </div>
                    @foreach ($loans as $id => $loan)
                        <div class="row col-md-12">
                            <div class="col-md-3">{{$loan->email}}</div>
                            <div class="col-md-2">{{$loan->amount_require}}</div>
                            <div class="col-md-2">{{$loan->is_approved}}</div>
                            <div class="col-md-2">
                                @if($loan->is_approved)
                                <button type="button" class="approve" onclick="repay({{$loan->id}})" data-approve="{{$loan->id}}">Repay</button>
                                @endif
                            </div>
                            <div class="col-md-3">
                                @if($loan->is_approved)
                                <input data-repay-loan="{{$loan->id}}" type="number" required/>
                                @endif
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
    <script>
        function repay(id) {
            var amount = $('[data-repay-loan='+id+']').val();
            $.ajax({
                type: 'POST',
                url: '/loan/repay',
                data: {loan_id: id, _token : '{{csrf_token()}}', amount: amount},
                success: function (data) {
                    alert(data.success)
                }
            });
        }
    </script>
@endsection
