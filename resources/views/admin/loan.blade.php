@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    <div class="row col-md-12">
                        <div class="col-md-4">Email</div>
                        <div class="col-md-4">Amount</div>
                        <div class="col-md-4">Approved</div>
                    </div>
                    @foreach ($loans as $id => $loan)
                        <div class="row col-md-12">
                            <div class="col-md-4">{{$loan->email}}</div>
                            <div class="col-md-3">{{$loan->amount_require}}</div>
                            <div class="col-md-3">{{$loan->is_approved}}</div>
                            <div class="col-md-2">
                                <button type="button" class="approve" onclick="approve({{$loan->id}})" data-approve="{{$loan->id}}">Approve</button>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
    <script>
        function approve(id) {
            $.ajax({
                type: 'POST',
                url: '/admin/loan/approve',
                data: {id: id, _token : '{{csrf_token()}}'},
                success: function (data) {
                    alert(data.success)
                }
            });
        }
    </script>
@endsection
