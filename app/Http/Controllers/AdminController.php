<?php

namespace App\Http\Controllers;

use App\Loan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function admin()
    {
        return view('admin.index');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function loan()
    {
        $loans = Loan::all();
        return view('admin.loan', ['loans' => $loans]);
    }

    /**
     * ajax request
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function approve(Request $request)
    {
        $input = $request->all();
        Loan::where('id', $input['id'])->update(
            [
                'is_approved' => 1,
                'approved_by' => Auth::user()->getAuthIdentifier()
            ]
        );

        return response()->json(['success'=>'Approved']);

    }
}
