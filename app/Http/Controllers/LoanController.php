<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoanRequest;
use App\Loan;
use App\Repay;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application loan.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $loans = Loan::all();
        return view('loan.index', ['loans' => $loans]);
    }

    /**
     * @param LoanRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function create(LoanRequest $request)
    {
        $validatedData = $request->validated();
        if($validatedData){
            $loan = new Loan();
            $loan->email = $request->input('email');
            $loan->amount_require = $request->input('amount_require');
            $loan->save();
        }

        return redirect('/loan');
    }

    public function loan()
    {
        $loans = Loan::where(['email' => Auth::user()->email])->get();

        return view('loan.loan', ['loans' => $loans]);
    }

    public function repay(Request $request)
    {
        $input = $request->all();
        if(empty($input['amount'])){
            return response()->json(['success'=>'Input amount']);
        }
        $repay = new Repay();
        $repay->user_pay = Auth::user()->id;
        $repay->amount = $input['amount'];
        $repay->loan_id = $input['loan_id'];
        $repay->save();

        return response()->json(['success'=>'Repay success']);
    }
}
