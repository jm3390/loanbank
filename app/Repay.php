<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Repay extends Model
{
    //
    protected $table = 'repay';

    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'amount', 'user_pay', 'loan_id'
    ];

}
